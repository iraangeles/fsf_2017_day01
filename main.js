//load our libraries
var express = require("express");
var path = require("path");

//console.info(">>> express: " + express);
// console.info(">>> path: " + path);

// Create an instance of express application 
var app = express();

// Define routes
app.use(express.static(__dirname + "/public"));

app.use(function(req, resp){
    resp.status(404);
    resp.type("text/html"); // representation 
    resp.send("<h1>File not found.</h1><p>The current time is " + new Date() + "</p>");
});


console.info (">>> dirname " + __dirname)

//Setting the port as a property of the app
app.set("port",3000);

console.info("Port: " + app.get("port"));

app.listen (app.get("port"), function(){
    console.info("Application is listening on port" + app.get("port"));
    console.info("Yaaah...");



});
